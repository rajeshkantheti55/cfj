import { Injectable, Injector } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class NoopInterceptor implements HttpInterceptor {

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {

        let headers = {
            url: req.url,
            setHeaders: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                id_token: `${localStorage.getItem('id_token')}`
            }
        }

        if (req.headers.has('X-Delete-ContentType')) {
            delete headers.setHeaders["Content-Type"]
        }

        const secureReq = req.clone(headers)

        return next.handle(secureReq).pipe(
            map((response: any) => {
                if (response instanceof HttpResponse) {
                    return response
                }
            }), catchError(error => {
                console.log(error)
                if (
                    error &&
                    error.error.message == "Unauthorized Admin"
                ) {
                    window.location.href = window.location.origin + '/unauthorized';
                }
                return throwError(error)
            })
        );

    }
}