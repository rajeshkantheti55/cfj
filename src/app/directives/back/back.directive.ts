import { Directive, HostListener } from '@angular/core';
import { Location } from '@angular/common';

@Directive({
    selector: '[BackButton]'
})
export class BackButtonDirective {

    constructor(
        private _Location: Location
    ) { }

    @HostListener('click')

    onClick() {
        this._Location.back();
    }

}
