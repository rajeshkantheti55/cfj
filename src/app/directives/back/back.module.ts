import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackButtonDirective } from './back.directive';

@NgModule({
  declarations: [
    BackButtonDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    BackButtonDirective
  ]
})
export class BackModule { }
