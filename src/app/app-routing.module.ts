import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './layouts/admin/admin.component';
import { AuthGuard } from './auth/auth.guard';
import { UnauthorizedComponent } from './auth/unauthorized/unauthorized.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

const routes: Routes = [
  { path: 'login', component: HomeComponent },
  { path: 'unauthorized', component: UnauthorizedComponent },
  {
    path: '', component: AdminComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'configuration', loadChildren: './pages/configurations/configurations.module#ConfigurationsModule' },
      { path: 'users', loadChildren: './pages/users/users.module#UsersModule' },
      { path: 'user-management', loadChildren: './pages/user-management/user-management.module#UserManagementModule' },
      { path: 'jobs', loadChildren: './pages/jobs/jobs.module#JobsModule' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
