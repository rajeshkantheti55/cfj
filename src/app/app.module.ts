import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './layouts/admin/admin.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonService } from './services/common.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpInterceptorProviders } from './_interceptors/index';
import { FooterComponent } from './components/footer/footer.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FilterPipe } from './services/pipes/filter.pipe';
import { UnauthorizedComponent } from './auth/unauthorized/unauthorized.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { en_US, NgZorroAntdModule, NZ_I18N, NZ_ICONS } from 'ng-zorro-antd';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import * as AllIcons from '@ant-design/icons-angular/icons'
import { IconDefinition } from '@ant-design/icons-angular';
import en from '@angular/common/locales/en';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

// import { ToastrModule } from 'ngx-toastr';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

registerLocaleData(en);

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key])

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdminComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    FilterPipe,
    UnauthorizedComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularMultiSelectModule,
    NgxJsonViewerModule,
    NgZorroAntdModule,
    BrowserAnimationsModule
    // ToastrModule.forRoot(),
  ],
  providers: [
    CommonService,
    HttpInterceptorProviders,
    FilterPipe,
    { provide: NZ_I18N, useValue: en_US },
    { provide: NZ_ICONS, useValue: icons }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
