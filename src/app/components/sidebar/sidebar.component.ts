import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoleService } from 'src/app/services/rbac/role.service';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  module: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'ni ni-shop', class: '', module: 'DASHBOARD' },
  { path: '/configuration/languages', title: 'Languages', icon: 'ni ni-bold', class: '', module: 'LANGUAGES' },
  { path: '/users', title: 'Users', icon: 'ni ni-single-02', class: '', module: 'USERS' },
  { path: '/user-management', title: 'User Management', icon: 'ni ni-single-02', class: '', module: 'USER_MANAGEMENT' },
]

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(
    private router: Router,
    private _RoleService: RoleService
  ) { }

  ngOnInit() {
    this.onGenerateMenuItems()
    this.onMenuToggle(this.router.url.split('/')[1])
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
  }

  onMenuToggle(route) {
    if (['jobs'].includes(route)) {
      this.onNavToggle('jobs-nav')
    }
  }

  onGenerateMenuItems() {
    this.menuItems = ROUTES.filter(menuItem => this._RoleService.hasReadAccess(menuItem.module));
  }

  onNavToggle(id) {
    let nav = document.getElementById(id)
    if (nav.className.indexOf('show') == -1) {
      nav.classList.add('show')
    } else {
      nav.classList.remove('show')
    }
  }

}
