import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTES } from '../sidebar/sidebar.component';
import { Location } from '@angular/common';
import { CommonService } from 'src/app/services/common.service';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public focus;
  public listTitles: any[];
  public location: Location;
  searchTermModel: any;


  constructor(
    location: Location,
    private _Router: Router,
    private _CommonService: CommonService
  ) {
    this.location = location;
  }

  ngOnInit() {
    this.listTitles = ROUTES.filter(listTitle => listTitle);
  }

  getTitle() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if (titlee.charAt(0) === '#') {
      titlee = titlee.slice(1);
    }

    for (var item = 0; item < this.listTitles.length; item++) {
      if (this.listTitles[item].path === titlee) {
        return this.listTitles[item].title;
      }
    }
    return 'Dashboard';
  }

  onLogout() {
    localStorage.clear()
    this._Router.navigate(["/login"]);
  }

  onSearch() {
    this._CommonService.get('users', { search: this.searchTermModel }).subscribe(response => {
      console.log(response.data)
    })
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(term =>
        this.searchGetCall(term).pipe(
          catchError(() => {
            return of([]);
          }))
      )
    )

  searchGetCall(term: string) {

    if (term === '') {
      return of([]);
    }

    return this._CommonService.get('users', { search: term }).pipe(map((response) => {
      return response['data'];
    }));

  }

  userPage(accountId) {
    this._Router.navigate(['users', accountId])
  }

}
