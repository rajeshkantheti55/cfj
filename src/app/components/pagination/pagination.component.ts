import { Component, Output, EventEmitter, Input, SimpleChange, OnChanges } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnChanges {

  @Input() totalRecords: number = 0;
  @Input() recordsPerPage: number = 0;
  @Input() options: any = {};
  @Output() onPageChange: EventEmitter<number> = new EventEmitter();

  pages: number[] = [];
  activePage: number;
  pageCount: any = 0

  ngOnChanges() {

    if (this.options.changePage) {
      this.pageCount = this.getPageCount();
      this.activePage = this.options.page
      this.createPageArray(this.activePage, this.recordsPerPage, this.totalRecords, 5)
      return this.onPageChange.emit(this.options.page);
    }

    this.pageCount = this.getPageCount();
    this.activePage = 1;
    this.createPageArray(this.activePage, this.recordsPerPage, this.totalRecords, 5)
    this.onPageChange.emit(1);

  }

  private getPageCount(): number {
    let totalPage: number = 0;
    if (this.totalRecords > 0 && this.recordsPerPage > 0) {
      const pageCount = this.totalRecords / this.recordsPerPage;
      const roundedPageCount = Math.floor(pageCount);
      totalPage = roundedPageCount < pageCount ? roundedPageCount + 1 : roundedPageCount;
    }

    return totalPage;
  }

  onClickPage(pageNumber: number) {
    if (pageNumber < 1) return;
    if (pageNumber > Math.ceil(this.totalRecords / this.recordsPerPage)) return;
    this.activePage = pageNumber;
    this.onPageChange.emit(this.activePage);
    // this.pageRange(this.activePage, this.pageCount)
    this.createPageArray(this.activePage, this.recordsPerPage, this.totalRecords, 5)
  }

  pageRange(page, pageCount) {
    let start = page - 2
    let end = page + 2;
    if (end > pageCount) {
      start -= (end - pageCount);
      end = pageCount;
    }

    if (start <= 0) {
      end += ((start - 1) * (-1));
      start = 1;
    }

    end = end > pageCount ? pageCount : end;

    let pageArray = []
    for (let i = start; i <= end; i++) {
      pageArray.push(i)
    }

    this.pages = pageArray;

  }

  calculatePageNumber(i: number, currentPage: number, paginationRange: number, totalPages: number) {
    let halfWay = Math.ceil(paginationRange / 2);
    if (i === paginationRange) {
      return totalPages;
    } else if (i === 1) {
      return i;
    } else if (paginationRange < totalPages) {
      if (totalPages - halfWay < currentPage) {
        return totalPages - paginationRange + i;
      } else if (halfWay < currentPage) {
        return currentPage - halfWay + i;
      } else {
        return i;
      }
    } else {
      return i;
    }
  }

  createPageArray(currentPage: number, itemsPerPage: number, totalItems: number, paginationRange: number) {
    // paginationRange could be a string if passed from attribute, so cast to number.
    paginationRange = +paginationRange;
    let pages = [];
    const totalPages = Math.ceil(totalItems / itemsPerPage);
    const halfWay = Math.ceil(paginationRange / 2);

    const isStart = currentPage <= halfWay;
    const isEnd = totalPages - halfWay < currentPage;
    const isMiddle = !isStart && !isEnd;

    let ellipsesNeeded = paginationRange < totalPages;
    let i = 1;

    while (i <= totalPages && i <= paginationRange) {
      let label;
      let pageNumber = this.calculatePageNumber(i, currentPage, paginationRange, totalPages);
      let openingEllipsesNeeded = (i === 2 && (isMiddle || isEnd));
      let closingEllipsesNeeded = (i === paginationRange - 1 && (isMiddle || isStart));
      if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
        label = '...';
      } else {
        label = pageNumber;
      }
      pages.push({
        label: label,
        value: pageNumber
      });
      i++;
    }

    this.pages = pages;
  }


}
