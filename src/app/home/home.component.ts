import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  loginForm: FormGroup
  isLoading: Boolean = false
  loginModal

  errorData = {
    display: false,
    message: ''
  }

  constructor(
    private _CommonService: CommonService,
    private _Router: Router,
    private _FormBuilder: FormBuilder,
    private _NgbModal: NgbModal
  ) {
    if (localStorage.getItem("token")) {
      this._Router.navigate(["/"]);
    }
  }

  ngOnInit() {
    this.initForms()
  }

  initForms() {
    this.loginForm = this._FormBuilder.group({
      email: ['hr@callforjobs.in', [Validators.required, Validators.maxLength(30), this._CommonService.validators.space]],
      password: ['admin@3826', [Validators.required, Validators.maxLength(30), this._CommonService.validators.space]]
    })
  }

  onPopup(content) {
    this.loginModal = this._NgbModal.open(content, { ariaLabelledBy: 'modal-basic-title' })
    this.loginModal.result.then((result) => {
      this.errorData.display = false
    }, (reason) => {
      this.errorData.display = false
    });
  }

  onLogin() {

    this.isLoading = true
    this._CommonService.post('auth/signin', this.loginForm.value).subscribe((response: any) => {

      this.isLoading = false

      if (response.status) {
        this.loginModal.close()
        localStorage.setItem('token', response.data.token)
        localStorage.setItem('role', response.data.role)
        this._Router.navigate(["/"]);
      } else {
        this.errorData.display = true
        this.errorData.message = response.message
      }

    }, error => {
      this.isLoading = false
      this.errorData.display = true
      this.errorData.message = error.error.message
    })

  }

}
