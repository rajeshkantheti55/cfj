import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigurationsRoutingModule } from './configurations-routing.module';
import { LanguagesComponent } from './components/languages/languages.component';
import { DragulaModule } from 'ng2-dragula';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BackModule } from 'src/app/directives/back/back.module';
import { LanguagesDataComponent } from './components/languages-data/languages-data.component';
import { LanguagesMultiDataComponent } from './components/languages-multi-data/languages-multi-data.component';
import { PaginationModule } from '../../components/pagination/pagination.module';

@NgModule({
  declarations: [LanguagesComponent, LanguagesDataComponent, LanguagesMultiDataComponent],
  imports: [
    CommonModule,
    ConfigurationsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DragulaModule.forRoot(),
    BackModule,
    PaginationModule
  ]
})
export class ConfigurationsModule { }
