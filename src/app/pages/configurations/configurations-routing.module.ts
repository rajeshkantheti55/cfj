import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LanguagesDataComponent } from './components/languages-data/languages-data.component';
import { LanguagesMultiDataComponent } from './components/languages-multi-data/languages-multi-data.component';
import { LanguagesComponent } from './components/languages/languages.component';

const routes: Routes = [
  { path: 'languages', component: LanguagesComponent },
  { path: 'languages/:id', component: LanguagesDataComponent },
  { path: 'languages/:language/:id/data', component: LanguagesMultiDataComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationsRoutingModule { }
