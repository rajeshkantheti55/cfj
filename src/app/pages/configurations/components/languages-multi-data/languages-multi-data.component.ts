import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-languages-multi-data',
  templateUrl: './languages-multi-data.component.html',
  styleUrls: ['./languages-multi-data.component.css']
})
export class LanguagesMultiDataComponent implements OnInit {

  id
  language
  edit: Boolean = false
  editData: any = {}

  isLoading: Boolean = false
  isLoadingData: Boolean = false

  languageForm: FormGroup
  languagesData: any = []
  languageInfoData: any = []
  tagsModal: any

  successData = {
    display: false,
    message: ''
  }

  errorData = {
    display: false,
    message: ''
  }

  constructor(
    private _NgbModal: NgbModal,
    private _CommonService: CommonService,
    private _FormBuilder: FormBuilder,
    private _ActivatedRoute: ActivatedRoute
  ) {
    _ActivatedRoute.params.subscribe(params => {
      this.id = params.id
      this.language = params.language
    })
  }


  ngOnInit() {
    this.onGetData()
    this.onGetLanguageData()
    this.initForms()
  }

  initForms() {
    this.languageForm = this._FormBuilder.group({
      key: ['', [Validators.required, this._CommonService.validators.space]],
      value: ['', [Validators.required, this._CommonService.validators.space]]
    })
  }

  onPopup(content) {
    this.tagsModal = this._NgbModal.open(content, { ariaLabelledBy: 'modal-basic-title' })
    this.tagsModal.result.then((result) => {
      this.successData.display = false
      this.errorData.display = false
      this.languageForm.reset()
      this.onGetLanguageData()
    }, (reason) => {
      this.successData.display = false
      this.errorData.display = false
      this.languageForm.reset()
    });
  }

  onCreateTag(form) {

    this.isLoading = true
    this.successData.display = false
    this.errorData.display = false

    if (this.edit) {
      this.languagesData.value[this.editData.index] = form
    } else {
      this.languagesData.value.push(form)
    }

    this._CommonService.put(`languages/data/${this.id}`, { value: this.languagesData.value }).subscribe(response => {
      this.isLoading = false
      this.successData.display = true
      this.successData.message = response.message
      this.tagsModal.close()
    }, error => {
      console.log(error)
      this.isLoading = false
      this.errorData.display = true
      this.errorData.message = error.error.message
    })

  }

  onGetData() {
    this.isLoadingData = true
    this._CommonService.get(`languages/${this.language}/info`).subscribe(response => {
      this.isLoadingData = false
      this.languageInfoData = response.data
    })
  }

  onGetLanguageData() {
    this.isLoadingData = true
    this._CommonService.get(`languages/data/${this.id}/info`).subscribe(response => {
      this.isLoadingData = false
      this.languagesData = response.data
    })
  }

  onDelete(index) {
    if (confirm('Are you sure to delete?')) {
      this.languagesData.value.splice(index, 1)
      this._CommonService.put(`languages/data/${this.id}`, { value: this.languagesData.value }).subscribe(response => {
        this.onGetLanguageData()
      }, error => {

      })
    }
  }

  onEdit(item, content, index) {
    this.edit = true
    this.editData = JSON.parse(JSON.stringify(item))
    this.editData.index = index
    this.languageForm.patchValue({ key: item.key, value: item.value })
    this.tagsModal = this._NgbModal.open(content, { ariaLabelledBy: 'modal-basic-title' })
    this.tagsModal.result.then((result) => {
      this.edit = false
      this.successData.display = false
      this.errorData.display = false
      this.languageForm.reset()
      this.onGetLanguageData()
    }, (reason) => {
      this.edit = false
      this.successData.display = false
      this.errorData.display = false
      this.languageForm.reset()
    });
  }

}
