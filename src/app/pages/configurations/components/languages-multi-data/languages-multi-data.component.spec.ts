import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguagesMultiDataComponent } from './languages-multi-data.component';

describe('LanguagesMultiDataComponent', () => {
  let component: LanguagesMultiDataComponent;
  let fixture: ComponentFixture<LanguagesMultiDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LanguagesMultiDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguagesMultiDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
