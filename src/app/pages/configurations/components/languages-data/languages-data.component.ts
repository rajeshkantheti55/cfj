import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-languages-data',
  templateUrl: './languages-data.component.html',
  styleUrls: ['./languages-data.component.css']
})
export class LanguagesDataComponent implements OnInit {

  language
  edit: Boolean = false
  editData: any = {}

  isLoading: Boolean = false
  isLoadingData: Boolean = false

  languageForm: FormGroup
  languagesData: any = []
  languageData: any = []
  tagsModal: any

  successData = {
    display: false,
    message: ''
  }

  errorData = {
    display: false,
    message: ''
  }

  totalCount = 0
  paginationData = {
    pageNo: 1,
    pageSize: 10,
    searchText: ''
  }

  constructor(
    private _NgbModal: NgbModal,
    private _CommonService: CommonService,
    private _FormBuilder: FormBuilder,
    private _ActivatedRoute: ActivatedRoute
  ) {
    _ActivatedRoute.params.subscribe(params => {
      this.language = params.id
    })
  }

  ngOnInit() {
    this.onGetData()
    this.onGetLanguageData()
    this.initForms()
  }

  get valueArray(): FormArray {
    return this.languageForm.get('value') as FormArray;
  }

  initValueFormGroup() {
    return this._FormBuilder.group({ key: [''], value: [''] })
  }

  initForms() {
    this.languageForm = this._FormBuilder.group({
      key: ['', [Validators.required, this._CommonService.validators.space]],
      value: ['', [Validators.required, this._CommonService.validators.space]],
      isArray: [false],
    })
  }

  onPopup(content) {
    this.tagsModal = this._NgbModal.open(content, { ariaLabelledBy: 'modal-basic-title' })
    this.tagsModal.result.then((result) => {
      this.successData.display = false
      this.errorData.display = false
      this.languageForm.reset()
      this.onGetLanguageData()
    }, (reason) => {
      this.successData.display = false
      this.errorData.display = false
      this.languageForm.reset()
    });
  }

  onCreateTag(form) {

    this.isLoading = true
    this.successData.display = false
    this.errorData.display = false

    if (this.edit) return this.onUpdate(form)

    this._CommonService.post(`languages/data`, { language: this.language, ...form }).subscribe(response => {
      this.isLoading = false
      this.successData.display = true
      this.successData.message = response.message
      this.tagsModal.close()
    }, error => {
      console.log(error)
      this.isLoading = false
      this.errorData.display = true
      this.errorData.message = error.error.message
    })

  }

  onUpdate(form) {
    this._CommonService.put(`languages/data/${this.editData._id}`, form).subscribe(response => {
      this.isLoading = false
      this.successData.display = true
      this.successData.message = response.message
      this.tagsModal.close()
    }, error => {
      console.log(error)
      this.isLoading = false
      this.errorData.display = true
      this.errorData.message = error.error.message
    })
  }

  onGetData() {
    this.isLoadingData = true
    this._CommonService.get(`languages/${this.language}/info`).subscribe(response => {
      this.isLoadingData = false
      this.languageData = response.data
    })
  }
  
  onGetLanguageData() {
    this.isLoadingData = true
    this._CommonService.get(`languages/data/${this.language}`, this.paginationData).subscribe(response => {
      this.isLoadingData = false
      this.languagesData = response.data
      this.totalCount = response.total
    })
  }

  onDelete(id) {
    if (confirm('Are you sure to delete?')) {
      this._CommonService.delete(`languages/data/${id}`).subscribe(response => {
        this.onGetLanguageData()
      }, error => {

      })
    }
  }

  onEdit(item, content) {
    this.edit = true
    this.editData = JSON.parse(JSON.stringify(item))
    this.initForms()
    this.languageForm.patchValue({ key: item.key })
    if (this.editData.isArray) {
      this.languageForm.patchValue({ isArray: true })
      this.onPatchResources(item)
    } else {
      this.languageForm.patchValue({ value: item.value, isArray: false })
    }
    this.tagsModal = this._NgbModal.open(content, { ariaLabelledBy: 'modal-basic-title' })
    this.tagsModal.result.then((result) => {
      this.edit = false
      this.successData.display = false
      this.errorData.display = false
      this.initForms()
      this.onGetLanguageData()
    }, (reason) => {
      this.edit = false
      this.successData.display = false
      this.errorData.display = false
      this.initForms()
    });
  }

  onPatchResources(data) {

    let value = new FormArray([])

    for (const iterator of data.value) {
      value.push(this._FormBuilder.group({ key: iterator.key, value: iterator.value }))
    }

    this.languageForm.setControl('value', value)

  }

  onValueTypeToggle() {
    if (this.languageForm.value.isArray) {
      this.languageForm.setControl("value", this._FormBuilder.array([this.initValueFormGroup()]))
    } else {
      this.languageForm.setControl("value", this._FormBuilder.control('', [Validators.required, this._CommonService.validators.space]))
    }
  }

  onAddKV() {
    this.valueArray.push(this.initValueFormGroup())
  }

  onRemoveKV(index) {
    const control = <FormArray>this.languageForm.get('value');
    if (control.length == 1) return
    control.removeAt(index);
  }

  onPageChange(page) {
    if (this.paginationData.pageNo != page) {
      this.paginationData.pageNo = page
      this.onGetLanguageData()
    }
  }

}
