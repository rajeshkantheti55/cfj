import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguagesDataComponent } from './languages-data.component';

describe('LanguagesDataComponent', () => {
  let component: LanguagesDataComponent;
  let fixture: ComponentFixture<LanguagesDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LanguagesDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguagesDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
