import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {


  isLoadingData: Boolean = false
  userId: string = ''
  userData: any = {}
  currentRoute = 1

  constructor(
    private _CommonService: CommonService,
    private _ActivatedRoute: ActivatedRoute,
    private _NgbModal: NgbModal
  ) {
    this._ActivatedRoute.params.subscribe(params => {
      if (params.id) {
        this.userId = params.id
        this.getUserDetails()
      }
    })
  }

  ngOnInit() {
  }

  getUserDetails() {
    this.isLoadingData = true
    this._CommonService.get(`users/${this.userId}`).subscribe(response => {
      this.isLoadingData = false
      this.userData = response.data
    })
  }

}
