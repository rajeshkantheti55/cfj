import { Component, OnInit } from '@angular/core';
// import { Chart, registerables } from 'chart.js'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  isLoadingData: Boolean = false
  analyticsData: any = {}
  charts = {
    one: null,
    two: null,
  }

  constructor() {
    // Chart.register(...registerables);
  }

  ngOnInit() {
  }

  // initCharts() {
  //   this.charts.one = new Chart("barone", {
  //     type: 'bar',
  //     data: {
  //       labels: [],
  //       datasets: [{
  //         data: [],
  //         backgroundColor: ["#53C3F6"],
  //         borderColor: ["#53C3F6"],
  //         borderWidth: 1
  //       }]
  //     },
  //     options: {
  //       plugins: {
  //         legend: { display: false }
  //       },
  //       scales: {
  //         y: {
  //           beginAtZero: true
  //         }
  //       }
  //     }
  //   });
  // }

}
