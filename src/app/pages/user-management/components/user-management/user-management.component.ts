import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {

  edit: Boolean = false
  editData: any = {}

  userForm: FormGroup
  usersData: any = []

  isLoading: Boolean = false
  isLoadingData: Boolean = false
  tagsModal: any

  successData = {
    display: false,
    message: ''
  }

  errorData = {
    display: false,
    message: ''
  }

  constructor(
    private _CommonService: CommonService,
    private _NgbModal: NgbModal,
    private _FormBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.onGetData()
    this.initForms()
  }

  initForms() {
    this.userForm = this._FormBuilder.group({
      email: ['', [Validators.required, Validators.maxLength(30), this._CommonService.validators.space]],
      role: ['ADMIN', [Validators.required, Validators.maxLength(30), this._CommonService.validators.space]]
    })
  }

  onGetData() {
    this.isLoadingData = true
    this._CommonService.get(`user-management`).subscribe(response => {
      this.isLoadingData = false
      this.usersData = response.data
    })
  }

  onPublish(id, block) {
    this._CommonService.put(`user-management/block/${id}`, { block }).subscribe(response => {
      this.onGetData()
    }, error => {

    })
  }

  onPopup(content) {
    this.tagsModal = this._NgbModal.open(content, { ariaLabelledBy: 'modal-basic-title' })
    this.tagsModal.result.then((result) => {
      this.successData.display = false
      this.errorData.display = false
      this.userForm.reset()
      this.onGetData()
    }, (reason) => {
      this.successData.display = false
      this.errorData.display = false
      this.userForm.reset()
    });
  }

  onUserCreate() {

    this.errorData.display = false
    this.successData.display = false

    this._CommonService.post(`user-management`, this.userForm.value).subscribe(response => {
      if (response.status) {
        this.successData.display = true
        this.successData.message = response.message
      } else {
        this.errorData.display = true
        this.errorData.message = response.message
      }
    }, error => {
      this.errorData.display = true
      this.errorData.message = error.error.message
    })
  }

}
