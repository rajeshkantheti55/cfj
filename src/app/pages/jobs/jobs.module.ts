import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobsRoutingModule } from './jobs-routing.module';
import { JobsComponent } from './components/jobs/jobs.component';
import { CreateJobsComponent } from './components/create-jobs/create-jobs.component';
import { DragulaModule } from 'ng2-dragula';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'src/app/components/pagination/pagination.module';
import { BackModule } from 'src/app/directives/back/back.module';
import { CategoryComponent } from './components/category/category.component';
import { CreateCategoryComponent } from './components/category/create-category/create-category.component';
import { ApplicantsComponent } from './components/applicants/applicants.component';
import { ManageApplicantsComponent } from './components/applicants/manage-applicants/manage-applicants.component';

@NgModule({
  declarations: [
    JobsComponent,
    CreateJobsComponent,
    CategoryComponent,
    CreateCategoryComponent,
    ApplicantsComponent,
    ManageApplicantsComponent
  ],
  imports: [
    CommonModule,
    JobsRoutingModule,
    DragulaModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    BackModule,
    PaginationModule
  ]
})
export class JobsModule { }
