import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-manage-applicants',
  templateUrl: './manage-applicants.component.html',
  styleUrls: ['./manage-applicants.component.css']
})
export class ManageApplicantsComponent implements OnInit {

  id: any
  edit: Boolean = false
  isLoading: Boolean = false
  isLoadingData: Boolean = false

  applicantForm: FormGroup
  applicantData: any = {}

  successData = {
    display: false,
    message: ''
  }

  errorData = {
    display: false,
    message: ''
  }

  constructor(
    private _FormBuilder: FormBuilder,
    private _CommonService: CommonService,
    private _ActivatedRoute: ActivatedRoute,
    private _Router: Router
  ) {
    _ActivatedRoute.params.subscribe(params => {
      if (params.id) {
        this.edit = this._ActivatedRoute.snapshot.data.update
        this.id = params.id
        this.onGetData()
      }
    })
  }

  ngOnInit() {
    this.initForms()
  }

  initForms() {
    this.applicantForm = this._FormBuilder.group({
      status: ['', [Validators.required]],
      comments: ['', [Validators.required]],
    })
  }

  onGetData() {
    this.isLoadingData = true
    this._CommonService.get(`jobs/application/${this.id}`).subscribe(response => {
      this.isLoadingData = false
      this.applicantData = response.data
    })
  }

  onFormSubmit() {
  }

}
