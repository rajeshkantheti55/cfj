import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-applicants',
  templateUrl: './applicants.component.html',
  styleUrls: ['./applicants.component.css']
})
export class ApplicantsComponent implements OnInit {

  edit: Boolean = false
  editData: any = {}

  isLoading: Boolean = false
  isLoadingData: Boolean = false

  usersData: any = []

  successData = {
    display: false,
    message: ''
  }

  errorData = {
    display: false,
    message: ''
  }

  constructor(
    private _CommonService: CommonService
  ) { }

  ngOnInit() {
    this.onGetData()
  }

  onGetData() {
    this.isLoadingData = true
    this._CommonService.get(`jobs/applicants/list`).subscribe(response => {
      this.isLoadingData = false
      this.usersData = response.data
    })
  }

  onPublish(id, verify) {
    this._CommonService.put(`users/verify/${id}`, { verify }).subscribe(response => {
      this.onGetData()
    }, error => {

    })
  }

}
