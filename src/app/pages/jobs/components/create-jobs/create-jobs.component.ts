import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-create-jobs',
  templateUrl: './create-jobs.component.html',
  styleUrls: ['./create-jobs.component.css']
})
export class CreateJobsComponent implements OnInit {

  id: any
  edit: Boolean = false
  isLoadingData: Boolean = false

  fileData: any
  imagePreview: any
  categories: any
  jobsForm: FormGroup

  isLoadingCategory: Boolean = false
  isLoading: Boolean = false

  successData = {
    display: false,
    message: ''
  }

  errorData = {
    display: false,
    message: ''
  }

  constructor(
    private _FormBuilder: FormBuilder,
    private _CommonService: CommonService,
    private _ActivatedRoute: ActivatedRoute,
    private _Router: Router
  ) {
    _ActivatedRoute.params.subscribe(params => {
      if (params.id) {
        this.edit = this._ActivatedRoute.snapshot.data.update
        this.id = params.id
        this.onGetData()
      }
    })
  }

  ngOnInit() {
    this.initForms()
    this.onGetCategoryData()
  }

  initForms() {
    this.jobsForm = this._FormBuilder.group({
      job_id: ['', [Validators.required]],
      company_name: ['', [Validators.required]],
      category: ['', [Validators.required]],
      company_location: ['', [Validators.required]],
      company_logo: ['', [Validators.required]],
      minimum_qualification: ['', [Validators.required]],
      job_experience: ['', [Validators.required]],
      job_profile: ['', [Validators.required]],
      job_description: ['', [Validators.required]]
    })
  }

  onGetCategoryData() {
    this.isLoadingCategory = true
    this._CommonService.get(`jobs/category`).subscribe(response => {
      this.isLoadingCategory = false
      this.categories = response.data
    })
  }

  onGetData() {
    this.isLoadingData = true
    this._CommonService.get(`jobs/${this.id}`).subscribe(response => {
      this.isLoadingData = false
      let data = response.data
      this.imagePreview = data.company_logo
      this.jobsForm.get('company_logo').clearValidators()
      this.jobsForm.patchValue({
        job_id: data.job_id,
        company_name: data.company_name,
        category: data.category,
        company_location: data.company_location,
        minimum_qualification: data.minimum_qualification,
        job_experience: data.job_experience,
        job_profile: data.job_profile,
        job_description: data.job_description
      })
    }, error => {
      this.isLoadingData = false
    })
  }

  onFormSubmit() {

    let formData: FormData = new FormData()

    formData.append('job_id', this.jobsForm.value.job_id)
    formData.append('company_name', this.jobsForm.value.company_name)
    formData.append('company_location', this.jobsForm.value.company_location)
    formData.append('minimum_qualification', this.jobsForm.value.minimum_qualification)
    formData.append('job_experience', this.jobsForm.value.job_experience)
    formData.append('job_profile', this.jobsForm.value.job_profile)
    formData.append('job_description', this.jobsForm.value.job_description)
    formData.append('category', this.jobsForm.value.category)

    if (this.fileData) {
      formData.append('company_logo', this.fileData)
    }

    this.isLoading = true
    this.successData.display = false
    this.errorData.display = false

    let route = this.edit ? `jobs/${this.id}` : 'jobs'
    let verb = this.edit ? 'put' : 'post'

    this._CommonService.upload[verb](route, formData).subscribe(response => {
      this.isLoading = false
      this.successData.display = true
      this.successData.message = response.message
      this._Router.navigate(['/jobs'])
    }, error => {
      console.log(error)
      this.isLoading = false
      this.errorData.display = true
      this.errorData.message = error.error.message
    })

  }

  onFileSelect(fileInput: any) {
    if (fileInput.files[0]) {
      this.fileData = fileInput.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(this.fileData);
      reader.onload = (ev: any) => {
        this.imagePreview = reader.result;
      };
    }
  }

}
