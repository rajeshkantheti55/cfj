import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DragulaOptions, DragulaService } from 'ng2-dragula';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  edit: Boolean = false
  editData: any = {}

  isLoading: Boolean = false
  isLoadingData: Boolean = false

  categoryForm: FormGroup
  languagesData: any = []
  tagsModal: any

  successData = {
    display: false,
    message: ''
  }

  errorData = {
    display: false,
    message: ''
  }

  showSaveBTN: Boolean = false
  DSubs = new Subscription();
  dragulaOptions: DragulaOptions = {
    direction: 'horizontal'
  }

  constructor(
    private _NgbModal: NgbModal,
    private _CommonService: CommonService,
    private _FormBuilder: FormBuilder,
    private _DragulaService: DragulaService
  ) { }

  ngOnInit() {
    this.onGetData()
    this.initForms()
    this.onDrag()
  }

  initForms() {
    this.categoryForm = this._FormBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(30), this._CommonService.validators.space]]
    })
  }

  onPopup(content) {
    this.tagsModal = this._NgbModal.open(content, { ariaLabelledBy: 'modal-basic-title' })
    this.tagsModal.result.then((result) => {
      this.successData.display = false
      this.errorData.display = false
      this.categoryForm.reset()
      this.onGetData()
    }, (reason) => {
      this.successData.display = false
      this.errorData.display = false
      this.categoryForm.reset()
    });
  }

  onCreateTag(form) {

    this.isLoading = true
    this.successData.display = false
    this.errorData.display = false

    if (this.edit) return this.onUpdate(form)

    this._CommonService.post(`jobs/category`, form).subscribe(response => {
      this.isLoading = false
      this.successData.display = true
      this.successData.message = response.message
      this.tagsModal.close()
    }, error => {
      console.log(error)
      this.isLoading = false
      this.errorData.display = true
      this.errorData.message = error.error.message
    })

  }

  onUpdate(form) {
    this._CommonService.put(`jobs/category/${this.editData._id}`, form).subscribe(response => {
      this.isLoading = false
      this.successData.display = true
      this.successData.message = response.message
      this.tagsModal.close()
    }, error => {
      console.log(error)
      this.isLoading = false
      this.errorData.display = true
      this.errorData.message = error.error.message
    })
  }

  onGetData() {
    this.isLoadingData = true
    this._CommonService.get(`jobs/category`).subscribe(response => {
      this.isLoadingData = false
      this.languagesData = response.data
    })
  }

  onDelete(id) {
    if (confirm('Are you sure to delete?')) {
      this._CommonService.delete(`jobs/category/${id}`).subscribe(response => {
        this.onGetData()
      }, error => {

      })
    }
  }

  onPublish(id, publish) {
    this._CommonService.put(`jobs/category/publish/${id}`, { publish }).subscribe(response => {
      this.onGetData()
    }, error => {

    })
  }

  onEdit(item, content) {
    this.edit = true
    this.editData = JSON.parse(JSON.stringify(item))
    this.categoryForm.patchValue({ name: item.name })
    this.tagsModal = this._NgbModal.open(content, { ariaLabelledBy: 'modal-basic-title' })
    this.tagsModal.result.then((result) => {
      this.edit = false
      this.successData.display = false
      this.errorData.display = false
      this.categoryForm.reset()
      this.onGetData()
    }, (reason) => {
      this.edit = false
      this.successData.display = false
      this.errorData.display = false
      this.categoryForm.reset()
    });
  }

  onDrag() {
    this.DSubs.add(this._DragulaService.dropModel('DCategory').subscribe(({ el, target, source, sourceModel, targetModel, item }) => {
      this.languagesData = sourceModel
      this.showSaveBTN = true
    }))
  }

  onSortData() {
    let index = 1
    let tags = this.languagesData.map(e => { return { _id: e._id, rank: index++ } })
    this.isLoadingData = true
    this._CommonService.put(`jobs/category/sort/data`, tags).subscribe(response => {
      this.isLoadingData = false
      this.showSaveBTN = false
      this.languagesData = response.data
    }, error => {
    })
  }

}
