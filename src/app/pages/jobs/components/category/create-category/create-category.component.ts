import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {

  id
  edit: Boolean = false
  categoryForm: FormGroup

  fileData: any
  imagePreview: any

  isLoading: Boolean = false
  isLoadingData: Boolean = false

  successData = {
    display: false,
    message: ''
  }

  errorData = {
    display: false,
    message: ''
  }

  constructor(
    private _CommonService: CommonService,
    private _FormBuilder: FormBuilder,
    private _ActivatedRoute: ActivatedRoute
  ) {
    _ActivatedRoute.params.subscribe(params => {
      if (params.id) {
        this.id = params.id
        this.onGetData()
      }
    })
  }

  ngOnInit() {
    this.initForms()
  }

  onGetData() {
    this.isLoadingData = true
    this._CommonService.get(`jobs/category/${this.id}`).subscribe(response => {

      let data = response.data

      this.isLoadingData = false
      this.imagePreview = data.thumbnail
      this.categoryForm.patchValue({ name: data.name })

    }, error => {
      this.isLoadingData = false
    })
  }

  initForms() {
    this.categoryForm = this._FormBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(30), this._CommonService.validators.space]],
      thumbnail: ['', [Validators.required]]
    })
  }

  onFileSelect(fileInput: any) {
    if (fileInput.files[0]) {
      this.fileData = fileInput.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(this.fileData);
      reader.onload = (ev: any) => {
        this.imagePreview = reader.result;
      };
    }
  }

  onCreateTag(form) {

    this.isLoading = true
    this.successData.display = false
    this.errorData.display = false

    let formData: FormData = new FormData()

    formData.append('name', this.categoryForm.value.name)
    formData.append('thumbnail', this.fileData)

    let route = this.edit ? `jobs/category/${this.id}/update` : `jobs/category`

    this._CommonService.upload.post(route, formData).subscribe(response => {
      this.isLoading = false
      this.successData.display = true
      this.successData.message = response.message
    }, error => {
      console.log(error)
      this.isLoading = false
      this.errorData.display = true
      this.errorData.message = error.error.message
    })

  }

}
