import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DragulaOptions, DragulaService } from 'ng2-dragula';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

  edit: Boolean = false
  editData: any = {}

  isLoading: Boolean = false
  isLoadingData: Boolean = false

  jobsForm: FormGroup
  jobsData: any = []

  successData = {
    display: false,
    message: ''
  }

  errorData = {
    display: false,
    message: ''
  }

  showSaveBTN: Boolean = false
  DSubs = new Subscription();
  dragulaOptions: DragulaOptions = {
    direction: 'horizontal'
  }

  constructor(
    private _CommonService: CommonService,
    private _FormBuilder: FormBuilder,
    private _DragulaService: DragulaService
  ) { }

  ngOnInit() {
    this.onGetData()
    this.initForms()
    this.onDrag()
  }

  initForms() {
    this.jobsForm = this._FormBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(30), this._CommonService.validators.space]]
    })
  }

  onGetData() {
    this.isLoadingData = true
    this._CommonService.get(`jobs`).subscribe(response => {
      this.isLoadingData = false
      this.jobsData = response.data
    })
  }

  onDelete(id) {
    if (confirm('Are you sure to delete?')) {
      this._CommonService.delete(`jobs/${id}`).subscribe(response => {
        this.onGetData()
      }, error => {

      })
    }
  }

  onPublish(id, publish) {
    this._CommonService.put(`jobs/publish/${id}`, { publish }).subscribe(response => {
      this.onGetData()
    }, error => {

    })
  }

  onDrag() {
    this.DSubs.add(this._DragulaService.dropModel('JTags').subscribe(({ el, target, source, sourceModel, targetModel, item }) => {
      this.jobsData = sourceModel
      this.showSaveBTN = true
    }))
  }

  onSortData() {
    let index = 1
    let tags = this.jobsData.map(e => { return { _id: e._id, rank: index++ } })
    this.isLoadingData = true
    this._CommonService.put(`jobs/sort/data`, tags).subscribe(response => {
      this.isLoadingData = false
      this.showSaveBTN = false
      this.jobsData = response.data
    }, error => {
    })
  }

}
