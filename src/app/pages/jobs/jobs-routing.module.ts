import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicantsComponent } from './components/applicants/applicants.component';
import { ManageApplicantsComponent } from './components/applicants/manage-applicants/manage-applicants.component';
import { CategoryComponent } from './components/category/category.component';
import { CreateCategoryComponent } from './components/category/create-category/create-category.component';
import { CreateJobsComponent } from './components/create-jobs/create-jobs.component';
import { JobsComponent } from './components/jobs/jobs.component';

const routes: Routes = [
  { path: '', component: JobsComponent },
  { path: 'create', component: CreateJobsComponent },
  { path: ':id/update', component: CreateJobsComponent, data: { update: true } },
  { path: ':id/view', component: CreateJobsComponent, data: { update: false } },
  { path: 'category', component: CategoryComponent },
  { path: 'category/create', component: CreateCategoryComponent },
  { path: 'category/:id/update', component: CreateCategoryComponent },
  { path: 'applicants', component: ApplicantsComponent },
  { path: 'applicants/:id/manage', component: ManageApplicantsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobsRoutingModule { }
