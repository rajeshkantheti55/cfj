import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';

const url: string = environment.url;

@Injectable()
export class CommonService {

  constructor(
    private _HttpClient: HttpClient
  ) { }

  get(route: string, params = {}): Observable<any> {
    return this._HttpClient.get(`${url}${route}`, { params })
  }

  post(route: string, body = {}, params = {}): Observable<any> {
    return this._HttpClient.post(`${url}${route}`, body, { params })
  }

  put(route: string, body = {}, params = {}): Observable<any> {
    return this._HttpClient.put(`${url}${route}`, body, { params })
  }

  delete(route: string, params = {}): Observable<any> {
    return this._HttpClient.delete(`${url}${route}`, { params })
  }

  upload = {
    post: (route: string, body = {}, params = {}): Observable<any> => {
      const headers = new HttpHeaders().set('X-Delete-ContentType', 'true');
      return this._HttpClient.post(`${url}${route}`, body, { params, headers })
    },
    put: (route: string, body = {}, params = {}): Observable<any> => {
      const headers = new HttpHeaders().set('X-Delete-ContentType', 'true');
      return this._HttpClient.put(`${url}${route}`, body, { params, headers })
    },
  }

  download = {
    GET: (route: any) => {
      return this._HttpClient.get(`${url}${route}`, { responseType: 'blob' })
    },
    POST: (route: any, body = {}) => {
      return this._HttpClient.post(`${url}${route}`, body, { responseType: 'blob' })
    }
  }

  validators = {
    space: (control: FormControl) => {
      const isWhitespace = control.value ? control.value.startsWith(' ') : null;
      const isValid = !isWhitespace;
      return isValid ? null : { 'whitespace': true };
    },
    markFormGroupTouched(formGroup: FormGroup) {
      (<any>Object).values(formGroup.controls).forEach(control => {
        control.markAsTouched();
        if (control.errors) {
          let name = (Object.keys(control.parent.controls).find(key => control.parent.controls[key] === control));
        }
        if (control.controls) {
          this.markFormGroupTouched(control);
        }
      });
    }
  }

}