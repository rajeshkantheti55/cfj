import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';


@Injectable({
  providedIn: 'root'
})
export class RoleService {

  PermissionsMap = {
    HR: {
      DASHBOARD: { read: true, write: false },
      LANGUAGES: { read: true, write: true },
      JOBS: { read: true, write: true },
      USERS: { read: true, write: true },
      USER_MANAGEMENT: { read: false, write: false },
    },
    ADMIN: {
      DASHBOARD: { read: true, write: true },
      LANGUAGES: { read: true, write: true },
      JOBS: { read: true, write: true },
      USERS: { read: true, write: true },
      USER_MANAGEMENT: { read: true, write: true },
    }
  }

  constructor(
    private AuthService: AuthService
  ) { }

  hasReadAccess(module: any) {

    const userRole = this.AuthService.getUserRole()
    const permissions = this.PermissionsMap[userRole][module]

    if (permissions) {
      return permissions.read
    }

    return true

  }

  hasWriteAccess(module: any) {

    const userRole = this.AuthService.getUserRole()
    const permissions = this.PermissionsMap[userRole][module]

    if (permissions) {
      return permissions.write
    }

    return true

  }

}
