export enum Role {
    operation = 'OPERATION',
    support = 'CUSTOMER_SUPPORT',
    admin = 'ADMIN'
}