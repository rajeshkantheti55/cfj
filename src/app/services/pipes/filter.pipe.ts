import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      value = value.filter(e => {
        const name = e.name
        const type = new RegExp(args)
        if (name.match(type)) {
          return true
        }
        return false
      })
    }
    return value;
  }

}
