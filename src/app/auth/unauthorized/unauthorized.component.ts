import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.css']
})
export class UnauthorizedComponent implements OnInit {

  constructor(
    private _CommonService: CommonService
  ) { }

  ngOnInit() {
    this.onLogOut()
  }

  onLogOut() {
    this._CommonService.post('auth/logout').subscribe(response => {
      const { url } = response
      window.location.href = url
    }, error => {
      console.log(error)
    })
  }

}
