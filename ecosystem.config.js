module.exports = {
    apps: [{
        name: 'callforjobs',
        script: 'index.js',
        instances: 1,
        autorestart: true,
        watch: false,
        max_memory_restart: '2G',
        log_date_format: "YYYY-MM-DD HH:mm Z"
    }]
};