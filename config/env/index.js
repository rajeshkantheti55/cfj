const path = require('path')
const dotenv = require('dotenv')
const { parsed } = dotenv.config()

const config = {
    env: parsed.ENV,
    port: Number(parsed.PORT),
    database: {
        mongo: {
            uri: parsed.MONGO_URI,
            options: {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }
        }
    },
    sms: {
        authKey: parsed.MSG_KEY,
        senderId: parsed.MSG_SENDER_ID,
        route: parsed.MSG_ROUTE
    }
}

module.exports = config