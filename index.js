'use strict';

const utils = require('./server/utils');
const database = require('./server/database/mongo');
const server = require('./server');

const initialize = async () => {

    try {

        const utilsData = utils.initialize()

        await database.initialize(utilsData)
        await server.initialize(utilsData)

        console.log(`CALL FOR JOBS | server is up and running in ${utilsData.config.env.toUpperCase()} environment on port ${utilsData.config.port}`);

    } catch (error) {
        console.error('ALERT!', error)
    }

}; initialize()