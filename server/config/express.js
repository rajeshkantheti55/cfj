'use strict';

const express = require('express');
const compression = require('compression');
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');

// Auth purpose
const session = require('express-session');
const mongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');
const config = require('./environment');

module.exports = function (app) {

    const env = config.env;

    app.set('view engine', 'html');
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    app.use(compression());
    app.use(morgan('dev'));
    app.use(express.static(path.join(config.root, 'dist/')));
    app.set('appPath', 'dist/');
    app.set('view engine', 'html');
    app.use(session({
        secret: config.secrets.session,
        resave: true,
        saveUninitialized: true,
        store: new mongoStore({ mongooseConnection: mongoose.connection })
    }));

};
