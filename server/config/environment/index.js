'use strict';

const path = require('path')
const _ = require('lodash');
const dotenv = require('dotenv')
const { parsed } = dotenv.config()

const all = {
    env: parsed.ENV,
    root: path.normalize(__dirname + '/../../..'),
    port: process.env.PORT,
    mongo: {
        uri: parsed.MONGO_URI,
        options: {
            poolSize: 5,
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
    },
    AWS: {
        id: parsed.AWS_ID,
        key: parsed.AWS_KEY,
        bucket: parsed.AWS_BUCKET,
    }
};

module.exports = _.merge(all, require('./' + all.env + '.js'));