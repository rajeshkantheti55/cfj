'use strict';

const config = require('./environment');
const messages = require('../messages/index');

if (config.ENABLE_QUEUES == 'true') {
    console.log('Queue is set to true, RabbitMQ will be connected')
    messages.transaction()
} else {
    console.log('Queues are disabled, please check env')
}