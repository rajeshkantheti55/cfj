const AWS = require('aws-sdk')
const multer = require("multer");
const multerS3 = require('multer-s3');
const path = require('path')
const config = require('../../config/environment')

const AWSHandler = module.exports

AWS.config.update({
    accessKeyId: config.AWS.id,
    secretAccessKey: config.AWS.key
})

const s3 = new AWS.S3();

AWSHandler.S3Upload = (type) => {
    return multer({
        storage: multerS3({
            s3,
            bucket: config.AWS.bucket,
            acl: 'public-read',
            metadata: function (req, file, cb) {
                return cb(null, { fieldName: file.fieldname });
            },
            key: function (req, file, cb) {
                const filePath = `${config.env}/${type}/${Date.now().toString()}${path.extname(file.originalname)}`
                return cb(null, filePath);
            }
        })
    })
};

AWSHandler.assets = (req, res, next) => {

    const upload = this.S3Upload('assets').any()

    upload(req, res, function (error) {

        if (error instanceof multer.MulterError) {
            console.log(error)
            return res.status(200).json({ status: false, message: error })
        } else if (error) {
            console.log(error)
            return res.status(200).json({ status: false, message: error })
        }

        return next()

    })
};