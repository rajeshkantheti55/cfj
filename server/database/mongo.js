const mongoose = require('mongoose')

const initialize = async (input = {}) => {
    try {
        const { config } = input
        return await mongoose.connect(config.database.mongo.uri, config.database.mongo.options)
    } catch (error) {
        throw error
    }
}

module.exports = { initialize }