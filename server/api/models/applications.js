const mongoose = require("mongoose");

const schema = mongoose.Schema({
    isDeleted: { type: Boolean, default: false },
    mobile: { type: String },
    draft_id: { type: String },
    job: { type: String, ref: 'c-jobs' }
}, { timestamps: true });

module.exports = mongoose.model('c-applications', schema);