const mongoose = require("mongoose");

const schema = mongoose.Schema({
    id: { type: Number, default: 0 },
    name: { type: String },
    isDeleted: { type: Boolean, default: false },
    publish: { type: Boolean, default: false },
    rank: { type: Number, default: 0 }
}, { timestamps: true });

module.exports = mongoose.model('c-languages', schema);