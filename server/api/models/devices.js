const mongoose = require("mongoose");

const schema = mongoose.Schema({
    ad_id: { type: String },
    app_version: { type: Number },
    device_id: { type: String, index: true },
    device_name: { type: String },
    device_type: { type: Number },
    draft_id: { type: Number },
    fcm_token: { type: String },
    referral_link: { type: String },
    referred_by: { type: Number }
}, { timestamps: true });

module.exports = mongoose.model('c-devices', schema);