const mongoose = require("mongoose");

const schema = mongoose.Schema({
    type: { type: String, required: true },
    count: { type: Number, default: 1 }
}, { timestamps: true, strict: false });

module.exports = mongoose.model('c-counters', schema);