const mongoose = require("mongoose");

const schema = mongoose.Schema({
    isDeleted: { type: Boolean, default: false },
    publish: { type: Boolean, default: false },
    rank: { type: Number, default: 0 },
    job_id: { type: String },
    company_name: { type: String },
    category: { type: String },
    company_logo: { type: String },
    company_location: { type: String },
    minimum_qualification: { type: String },
    job_published_time: { type: String },
    job_experience: { type: String },
    job_profile: { type: String },
    job_description: { type: String }
}, { timestamps: true });

module.exports = mongoose.model('c-jobs', schema);