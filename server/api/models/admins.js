const mongoose = require("mongoose");

const schema = mongoose.Schema({
    email: { type: String, unique: true },
    role: { type: String, required: true },
    password: { type: String },
    isResetPasswordRequired: { type: Boolean, default: true },
    isBlocked: { type: Boolean, default: false }
}, { timestamps: true, strict: false, versionKey: false });

module.exports = mongoose.model('c-admins', schema);