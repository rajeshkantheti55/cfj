const mongoose = require("mongoose");

const schema = mongoose.Schema({
    type: { type: String, required: true },
    user: { type: String, required: true },
}, { timestamps: true, strict: false, versionKey: false });

module.exports = mongoose.model('c-preference', schema);