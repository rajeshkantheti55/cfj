const mongoose = require("mongoose");

const schema = mongoose.Schema({
    draft_id: { type: String },
    mobile: { type: String },
    otp: { type: Number },
}, { timestamps: true, strict: false, versionKey: false });

module.exports = mongoose.model('c-otps', schema);