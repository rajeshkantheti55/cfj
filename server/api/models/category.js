const mongoose = require("mongoose");

const schema = mongoose.Schema({
    isDeleted: { type: Boolean, default: false },
    publish: { type: Boolean, default: false },
    rank: { type: Number, default: 0 },
    name: { type: String },
    thumbnail: { type: String }
}, { timestamps: true });

module.exports = mongoose.model('c-category', schema);