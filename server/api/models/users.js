const mongoose = require("mongoose");

const schema = mongoose.Schema({
    draft_id: { type: String },
    firstName: { type: String },
    lastName: { type: String },
    mobile: { type: String },
    password: { type: String },
    education: {
        class: { type: String },
        specialization: { type: String },
    },
    professional: {
        designation: { type: String },
        experience: { type: String },
        company: { type: String },
        interests: { type: String }
    },
    location: {
        current: { type: String },
        preferred: { type: String }
    },
    isVerified: { type: Boolean, default: false }
}, { timestamps: true, strict: false, versionKey: false });

module.exports = mongoose.model('c-users', schema);