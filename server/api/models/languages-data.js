const mongoose = require("mongoose");

const schema = mongoose.Schema({
    isDeleted: { type: Boolean, default: false },
    language: { type: String, ref: 'c-languages' },
    key: { type: String },
    isArray: { type: Boolean, default: false }
}, { timestamps: true, strict: false });

module.exports = mongoose.model('c-languages-data', schema);