const router = require('express').Router();

router.use('/auth', require('./controllers/auth'));
router.use('/languages', require('./controllers/languages'));
router.use('/languages/data', require('./controllers/languages-data'));
router.use('/jobs', require('./controllers/jobs'));
router.use('/users', require('./controllers/users'));
router.use('/user-management', require('./controllers/user-management'));

module.exports = router