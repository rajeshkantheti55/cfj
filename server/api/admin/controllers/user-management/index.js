const router = require('express').Router()
const controller = require('./user-management.controller')

router.post('/', controller.create)
router.get('/', controller.list)
router.get('/:id', controller.listById)
router.put('/block/:id', controller.block)

module.exports = router
