const UserService = require('./user-management.service')

const controller = module.exports

controller.list = async (req, res) => {
    try {

        const { query } = req
        const data = await UserService.list(query)
        return res.status(200).json({ status: true, data })

    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })
    }
}

controller.create = async (req, res) => {
    try {

        const { body } = req
        const data = await UserService.create(body)

        return res.status(200).json({ status: true, message: `The user created successfully. User can login with the temporary password ${data.password}.` })

    } catch (error) {

        if (error == "DUPLICATE-CREATION") {
            return res.status(200).json({ status: false, message: "The email is already registed" })
        }

        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })
    }
}

controller.listById = async (req, res) => {
    try {

        const { params } = req
        const data = await UserService.listById(params.id)
        return res.status(200).json({ status: true, data })

    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })
    }
}

controller.block = async (req, res) => {
    try {
        const { user, body, params } = req
        const data = await UserService.block(params.id, body.block)
        return res.status(200).json({ statusCode: 200, message: 'The user is updated successfully!', data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}