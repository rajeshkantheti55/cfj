const Helper = require('../../../../helpers/index')
const AdminModel = require('../../../models/admins')
const service = module.exports

service.create = async (payload) => {
    try {

        const randomPassword = `${payload.role}@${Helper.RandomNumber(4)}`.toLowerCase()
        const data = {
            email: payload.email.trim(),
            role: payload.role.trim(),
            password: Helper.getPasswordHash(randomPassword)
        }

        await new AdminModel(data).save()
        return { password: randomPassword }

    } catch (error) {

        if (error.code == 11000) {
            throw "DUPLICATE-CREATION"
        }

        console.error(error)
        throw error

    }
}

service.list = async (payload) => {

    try {

        let userData = await AdminModel.find({}, {
            password: 0
        }).lean()

        return userData

    } catch (error) {
        throw error
    }

}

service.listById = async (userId) => {

    try {

        let userData = await AdminModel.findOne({ _id: userId }, {
            password: 0
        }).lean()

        return userData

    } catch (error) {
        throw error
    }

}

service.block = async (id, block) => {
    const findQuery = { _id: id }
    return await AdminModel.updateOne(findQuery, { $set: { isBlocked: block } })
}
