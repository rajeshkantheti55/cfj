const UserService = require('./users.service')

const controller = module.exports

controller.list = async (req, res) => {
    try {

        const { query } = req
        const data = await UserService.list(query)
        return res.status(200).json({ status: true, data })

    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })
    }
}

controller.listById = async (req, res) => {
    try {

        const { params } = req
        const data = await UserService.listById(params.id)
        return res.status(200).json({ status: true, data })

    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })
    }
}

controller.verify = async (req, res) => {
    try {
        const { user, body, params } = req
        const data = await UserService.verify(params.id, body.verify)
        return res.status(200).json({ statusCode: 200, message: 'The user is updated successfully!', data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}