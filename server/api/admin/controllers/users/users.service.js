const UsersModel = require('../../../models/users')
const service = module.exports

service.list = async (payload) => {

    try {

        let userData = await UsersModel.find({}, {
            password: 0
        }).lean()

        return userData

    } catch (error) {
        throw error
    }

}

service.listById = async (userId) => {

    try {

        let userData = await UsersModel.findOne({ _id: userId }, {
            password: 0
        }).lean()

        return userData

    } catch (error) {
        throw error
    }

}

service.verify = async (id, verify) => {
    const findQuery = { _id: id }
    return await UsersModel.updateOne(findQuery, { $set: { isVerified: verify } })
}
