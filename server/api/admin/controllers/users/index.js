const router = require('express').Router()
const controller = require('./users.controller')

router.get('/', controller.list)
router.get('/:id', controller.listById)
router.put('/verify/:id', controller.verify)

module.exports = router
