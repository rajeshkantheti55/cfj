const LanguageModel = require('../../../models/languages')
const service = module.exports

service.create = async (userInfo, body) => {
    return await new LanguageModel(body).save()
}

service.read = async () => {
    return LanguageModel.find({ isDeleted: false })
        .sort({ id: 1 })
        .lean()
}

service.info = async (id) => {
    return LanguageModel.findOne({ isDeleted: false, _id: id })
}

service.update = async (id, payload) => {
    const findQuery = { _id: id }
    return await LanguageModel.updateOne(findQuery, { $set: payload })
}

service.delete = async (id) => {
    const findQuery = { _id: id }
    const updateQuery = { isDeleted: true }
    return await LanguageModel.updateOne(findQuery, updateQuery)
}

service.sort = async (payload) => {
    for (const iterator of payload) {
        await LanguageModel.updateOne({ _id: iterator._id }, { $set: { id: iterator.id } })
    }
}

service.publish = async (id, payload) => {
    const findQuery = { _id: id }
    return await LanguageModel.updateOne(findQuery, { $set: payload })
}
