const service = require('./jobs.service')
const controller = module.exports

controller.create = async (req, res) => {
    try {
        const { user, body, file } = req
        const data = await service.create(user, body, file)
        return res.status(200).json({ statusCode: 200, message: 'The job is created successfully!', data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}

controller.read = async (req, res) => {
    try {
        const { user } = req
        const data = await service.read()
        return res.status(200).json({ statusCode: 200, status: true, data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}

controller.info = async (req, res) => {
    try {
        const { user, params } = req
        const data = await service.info(params.id)
        return res.status(200).json({ statusCode: 200, status: true, data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}

controller.update = async (req, res) => {
    try {
        const { user, body, params, file } = req
        const data = await service.update(params.id, body, file)
        return res.status(200).json({ statusCode: 200, message: 'The job is updated successfully!', data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}

controller.delete = async (req, res) => {
    try {
        const { user, params } = req
        const data = await service.delete(params.id)
        return res.status(200).json({ statusCode: 200, status: true, data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}

controller.sort = async (req, res) => {
    try {
        const { user, body } = req
        await service.sort(body)
        const data = await service.read()
        return res.status(200).json({ statusCode: 200, status: true, data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}

controller.publish = async (req, res) => {
    try {
        const { user, body, params } = req
        const data = await service.publish(params.id, body)
        return res.status(200).json({ statusCode: 200, message: 'The job is updated successfully!', data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}

controller.applicants = async (req, res) => {
    try {
        const { user, query } = req
        const data = await service.applicants(query)
        return res.status(200).json({ statusCode: 200, status: true, data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}

controller.application = async (req, res) => {
    try {
        const { user, params } = req
        const data = await service.application(params.id)
        return res.status(200).json({ statusCode: 200, status: true, data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}
