const CategoryModel = require('../../../../models/category')
const service = module.exports

service.create = async (userInfo, body, file) => {
    const data = {
        ...body,
        ...(file && file.location ? { thumbnail: file.location } : null)
    }
    return await new CategoryModel(data).save()
}

service.read = async () => {
    return CategoryModel.find({ isDeleted: false })
        .sort({ rank: 1 })
        .lean()
}

service.info = async (id) => {
    return CategoryModel.findOne({ _id: id, isDeleted: false })
        .sort({ rank: 1 })
        .lean()
}

service.update = async (id, body, file) => {
    const findQuery = { _id: id }
    const data = {
        ...body,
        ...(file && file.location ? { thumbnail: file.location } : null)
    }
    return await CategoryModel.updateOne(findQuery, { $set: data })
}

service.delete = async (id) => {
    const findQuery = { _id: id }
    const updateQuery = { isDeleted: true }
    return await CategoryModel.updateOne(findQuery, updateQuery)
}

service.sort = async (payload) => {
    for (const iterator of payload) {
        await CategoryModel.updateOne({ _id: iterator._id }, { $set: { rank: iterator.rank } })
    }
}

service.publish = async (id, payload) => {
    const findQuery = { _id: id }
    return await CategoryModel.updateOne(findQuery, { $set: payload })
}
