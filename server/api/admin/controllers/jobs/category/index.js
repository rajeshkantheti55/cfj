const router = require('express').Router()
const controller = require('./category.controller')
const { aws } = require('../../../../../cloud')

/**
 * 
 * @create
 * @read
 * @info
 * @update
 * @delete
 * @sort
 * @publish
 * 
 */

router.post('/', aws.S3Upload("jobs").single("thumbnail"), controller.create);
router.get('/', controller.read)
router.get('/:id', controller.info)
router.put('/:id', aws.S3Upload("jobs").single("thumbnail"), controller.update);
router.delete('/:id', controller.delete);
router.put('/sort/data', controller.sort);
router.put('/publish/:id', controller.publish);


module.exports = router
