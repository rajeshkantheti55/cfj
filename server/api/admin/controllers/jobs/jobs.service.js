const JobsModel = require('../../../models/jobs')
const JobsApplicationsModel = require('../../../models/applications')
const UsersModel = require('../../../models/users')
const service = module.exports

service.create = async (userInfo, body, file) => {
    const data = {
        ...body,
        ...(file && file.location ? { company_logo: file.location } : null)
    }
    return await new JobsModel(data).save()
}

service.read = async () => {
    return JobsModel.find({ isDeleted: false })
        .sort({ rank: 1 })
        .lean()
}

service.info = async (id) => {
    return JobsModel.findOne({ _id: id, isDeleted: false })
        .sort({ rank: 1 })
        .lean()
}

service.update = async (id, body, file) => {
    const findQuery = { _id: id }
    const data = {
        ...body,
        ...(file && file.location ? { company_logo: file.location } : null)
    }
    return await JobsModel.updateOne(findQuery, { $set: data })
}

service.delete = async (id) => {
    const findQuery = { _id: id }
    const updateQuery = { isDeleted: true }
    return await JobsModel.updateOne(findQuery, updateQuery)
}

service.sort = async (payload) => {
    for (const iterator of payload) {
        await JobsModel.updateOne({ _id: iterator._id }, { $set: { rank: iterator.rank } })
    }
}

service.publish = async (id, payload) => {
    const findQuery = { _id: id }
    return await JobsModel.updateOne(findQuery, { $set: payload })
}

service.applicants = async (queryParams) => {

    let jobs = await JobsApplicationsModel.find({}).populate('job').lean()

    for (const iterator of jobs) {
        iterator.user = user = await UsersModel.findOne({ mobile: iterator.mobile, draft_id: iterator.draft_id }).lean()
    }

    return jobs

}

service.application = async (id) => {

    let application = await JobsApplicationsModel.findOne({ _id: id }).populate('job').lean()

    application.user = user = await UsersModel.findOne({ mobile: application.mobile, draft_id: application.draft_id }).lean()

    return application

}