const router = require('express').Router()
const controller = require('./jobs.controller')
const { aws } = require('../../../../cloud')

router.use('/category', require('./category'));

/**
 * 
 * @create
 * @read
 * @info
 * @update
 * @delete
 * @sort
 * @publish
 * 
 */

router.post('/', aws.S3Upload("jobs").single("company_logo"), controller.create);
router.get('/', controller.read)
router.get('/:id', controller.info)
router.put('/:id', aws.S3Upload("jobs").single("company_logo"), controller.update);
router.delete('/:id', controller.delete);
router.put('/sort/data', controller.sort);
router.put('/publish/:id', controller.publish);

router.get('/applicants/list', controller.applicants)
router.get('/application/:id', controller.application)

module.exports = router
