const router = require('express').Router()
const controller = require('./languages.controller')

/**
 * 
 * @create
 * @read
 * @update
 * @delete
 * @sort
 * @publish
 * 
 */

router.post('/', controller.create);
router.get('/:language', controller.read)
router.get('/:id/info', controller.info)
router.put('/:id', controller.update);
router.delete('/:id', controller.delete);
router.put('/sort/data', controller.sort);
router.put('/publish/:id', controller.publish);


module.exports = router
