const LanguageDataModel = require('../../../models/languages-data')
const Helper = require('../../../../helpers/index')
const service = module.exports

service.create = async (userInfo, body) => {
    return await new LanguageDataModel(body).save()
}

service.read = async (language, query) => {
    const { pageNo, pageSize } = Helper.parsePageNoAndSize(query);
    const total = await LanguageDataModel.count({ isDeleted: false, language })
    const data = await LanguageDataModel.find({ isDeleted: false, language })
        .skip(pageNo * pageSize)
        .limit(pageSize)
        .lean()
    return { total, data }
}

service.info = async (id) => {
    return LanguageDataModel.findOne({ isDeleted: false, _id: id })
}

service.update = async (id, payload) => {
    const findQuery = { _id: id }
    return await LanguageDataModel.updateOne(findQuery, { $set: payload })
}

service.delete = async (id) => {
    const findQuery = { _id: id }
    const updateQuery = { isDeleted: true }
    return await LanguageDataModel.updateOne(findQuery, updateQuery)
}

service.sort = async (payload) => {
    for (const iterator of payload) {
        await LanguageDataModel.updateOne({ _id: iterator._id }, { $set: { id: iterator.id } })
    }
}

service.publish = async (id, payload) => {
    const findQuery = { _id: id }
    return await LanguageDataModel.updateOne(findQuery, { $set: payload })
}
