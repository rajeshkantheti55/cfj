const router = require('express').Router()
const controller = require('./auth.controller')

router.post('/signin', controller.signin)

// router.get('/', controller.list)
// router.get('/:id', controller.listById)
// router.put('/verify/:id', controller.verify)

module.exports = router
