const AuthService = require('./auth..service')

const controller = module.exports

controller.signin = async (req, res) => {
    try {

        const { body } = req
        const data = await AuthService.signin(body)
        return res.status(200).json({ status: true, data })

    } catch (error) {

        if (error == "UNREGISTERED") {
            return res.status(200).json({ status: false, message: "Uh Oh! Looks like the email is not registered." })
        }

        if (error == "BLOCKED") {
            return res.status(200).json({ status: false, message: "Unauthorized. Plase contact admin." })
        }

        if (error == "INVALID-PASSWORD") {
            return res.status(200).json({ status: false, message: "Invalid email or password" })
        }

        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })

    }
}
