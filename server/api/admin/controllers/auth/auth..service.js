const AdminModel = require('../../../models/admins')
const Helper = require('../../../../helpers/index')
const AuthHelper = require('../../../../helpers/auth.helper')
const service = module.exports

service.signin = async (payload) => {
    try {

        const userData = await AdminModel.findOne({ email: payload.email }).lean()

        if (!userData) {
            throw "UNREGISTERED"
        }

        if (userData.isBlocked) {
            throw "BLOCKED"
        }

        if (!Helper.verifyPasswordHash(payload.password, userData.password)) {
            throw "INVALID-PASSWORD"
        }

        const token = AuthHelper.createToken({ draft_id: userData.draft_id, mobile: userData.mobile })

        delete userData.password

        return { token, ...userData }

    } catch (error) {
        console.error(error)
        throw error
    }
}
