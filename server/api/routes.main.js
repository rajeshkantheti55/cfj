module.exports = (app) => {

    app.use('/api/app/v1', require('./app/v1'))
    app.use('/api/admin', require('./admin'))

    app.route('/health').get(function (req, res) {
        return res.status(200).json({
            statusCode: 200,
            message: 'Server is running successfully!'
        })
    })

    app.route('/*').get((request, response) => {
        try {
            return response.sendFile(app.get('client'));
        } catch (error) {
            console.error(error)
            return response.status(500).json({
                status: false,
                message: 'Internal Server Error',
                error
            })
        }
    })

}