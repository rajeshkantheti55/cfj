const router = require('express').Router();

router.use('/splash', require('./controllers/splash'));
router.use('/languages', require('./controllers/languages'));
router.use('/users', require('./controllers/users'));
router.use('/jobs', require('./controllers/jobs'));
router.use('/preferences', require('./controllers/preferences'));

module.exports = router