const service = require('./jobs.service')
const controller = module.exports

controller.read = async (req, res) => {
    try {
        const { user } = req
        const data = await service.read()
        return res.status(200).json({ statusCode: 200, status: true, data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}

controller.apply = async (req, res) => {
    try {
        const { user, body } = req
        const data = await service.apply(user, body)
        return res.status(200).json({ statusCode: 200, status: true, data })
    } catch (error) {

        console.error(error)

        if (error == 'APPLIED') {
            return res.status(200).json({ status: false, message: 'The job is applied already' })
        } else if (error == 'UNVERIFY') {
            return res.status(200).json({ status: false, message: 'The user is not verified.' })
        }

        return res.status(200).json({ status: false, message: 'Internal server error', error })

    }
}

controller.viewApplied = async (req, res) => {
    try {
        const { user } = req
        const data = await service.viewApplied(user)
        return res.status(200).json({ statusCode: 200, status: true, data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}
