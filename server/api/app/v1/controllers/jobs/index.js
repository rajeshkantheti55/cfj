const router = require('express').Router()
const controller = require('./jobs.controller')
const auth = require('../../../../../helpers/auth.helper')

/**
 * 
 * @read
 * 
 */

router.get('/', auth.authenticate, controller.read)
router.post('/apply', auth.authenticate, controller.apply)
router.get('/view/applied', auth.authenticate, controller.viewApplied)


module.exports = router
