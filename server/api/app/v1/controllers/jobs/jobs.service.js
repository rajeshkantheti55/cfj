const JobsModel = require('../../../../models/jobs')
const JobsApplicationsModel = require('../../../../models/applications')
const UserService = require('../users/users.service')
const service = module.exports

service.read = async () => {
    return JobsModel.find(
        { isDeleted: false, publish: true },
        {
            isDeleted: false,
            publish: false,
            createdAt: false,
            rank: 0,
            __v: 0
        })
        .sort({ rank: 1 })
        .lean()
}

service.apply = async (user, payload) => {

    const isVerified = await UserService.isVerified(user)

    if (!isVerified) {
        throw "UNVERIFY"
    }

    const isApplied = await JobsApplicationsModel.count({
        mobile: user.mobile,
        draft_id: user.draft_id,
        job: payload.job
    })

    if (isApplied) {
        throw "APPLIED"
    }

    return new JobsApplicationsModel({
        mobile: user.mobile,
        draft_id: user.draft_id,
        job: payload.job
    }).save()
}

service.viewApplied = async (user) => {
    return JobsApplicationsModel.find({ mobile: user.mobile, draft_id: user.draft_id })
        .populate('job')
        .lean()
}
