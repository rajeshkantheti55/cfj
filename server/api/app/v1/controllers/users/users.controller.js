const joi = require('joi')
const UserService = require('./users.service')
const Schemas = require('../../schemas')
const controller = module.exports

controller.register = async (req, res) => {
    try {

        const { body } = req
        const { error, value } = joi.validate(body, Schemas.user.save)

        if (error) {
            return res.status(400).json({ message: "Bad params", error })
        }

        const data = await UserService.save(value)

        return res.status(200).json({ status: true, data })

    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })
    }
}

controller.login = async (req, res) => {
    try {

        const { body } = req
        const { error, value } = joi.validate(body, Schemas.user.login)

        if (error) {
            return res.status(400).json({ message: "Bad params", error })
        }

        const data = await UserService.login(value)

        return res.status(200).json({ status: true, data, message: 'Login successfull' })

    } catch (error) {

        console.error(error)

        if (error == "Invalid email or password") {
            return res.status(200).json({ status: false, message: error })
        }

        return res.status(200).json({ status: false, message: "Internal server error", error })

    }
}

controller.logout = async (req, res) => {
    try {

        const { user } = req

        return res.status(200).json({ status: true, message: "Logout successfully" })

    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })
    }
}

controller.update = async (req, res) => {
    try {

        const { body } = req
        const { error, value } = joi.validate(body, Schemas.splash.update)

        if (error) {
            return res.status(400).json({ message: "Bad params", error })
        }

        const data = await UserService.update(value)

        return res.status(200).json({ status: true, data })

    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })
    }
}

controller.verify = async (req, res) => {
    try {

        const { user, body } = req
        const { error, value } = joi.validate(body, Schemas.user.verify)

        if (error) {
            return res.status(400).json({ message: "Bad params", error })
        }

        const data = await UserService.verify(user, value)

        return res.status(200).json({ status: true, data, message: "OTP sent successfully" })

    } catch (error) {

        console.error(error)

        if (error == 'FREQUENT') {
            return res.status(200).json({ status: false, message: "Please try again in sometime" })
        }

        return res.status(200).json({ status: false, message: "Internal server error", error })

    }
}

controller.validate = async (req, res) => {
    try {

        const { user, body } = req
        const { error, value } = joi.validate(body, Schemas.user.validate)

        if (error) {
            return res.status(400).json({ message: "Bad params", error })
        }

        const data = await UserService.validate(user, value)

        return res.status(200).json({ status: true, message: 'Verified successfully' })

    } catch (error) {

        console.error(error)

        if (error == "INVALID") {
            return res.status(200).json({ status: false, message: "OTP is invalid. Please enter correct OTP" })
        }

        return res.status(200).json({ status: false, message: "Internal server error", error })

    }
}