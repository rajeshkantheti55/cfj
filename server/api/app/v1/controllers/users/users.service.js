const moment = require('moment')
const UsersModel = require('../../../../models/users')
const OTPModel = require('../../../../models/otps')
const SMSService = require('../../../../../helpers/sms/index')
const Helper = require('../../../../../helpers/index')
const AuthHelper = require('../../../../../helpers/auth.helper')
const service = module.exports

service.save = async (payload) => {

    try {

        /**
         * 
         * 1. Check the user if already registered by draft_id or mobile 
         * 
         */

        let userData = await UsersModel.findOne({ $or: [{ draft_id: payload.draft_id }, { mobile: payload.mobile }] }).lean()

        if (userData) {
            return userData
        }

        payload.password = Helper.getPasswordHash(payload.password)
        userData = await new UsersModel(payload).save()

        const token = AuthHelper.createToken({ draft_id: userData.draft_id, mobile: userData.mobile })

        delete userData.password

        return { token, ...userData._doc }

    } catch (error) {
        throw error
    }

}

service.update = async (payload) => {

    try {

        let userData = await UsersModel.findOneAndUpdate(
            { draft_id: payload.draft_id },
            { $set: payload },
            { new: true }
        )

        return userData

    } catch (error) {
        throw error
    }

}

service.login = async (payload) => {

    try {

        const userData = await UsersModel.findOne({ mobile: payload.mobile }).lean()

        if (!Helper.verifyPasswordHash(payload.password, userData.password)) {
            throw "Invalid email or password"
        }

        const token = AuthHelper.createToken({ draft_id: userData.draft_id, mobile: userData.mobile })

        delete userData.password

        return { token, ...userData }

    } catch (error) {
        throw error
    }

}

service.verify = async (user, payload) => {

    try {

        const lastOTP = await OTPModel.findOne({ draft_id: payload.draft_id, mobile: payload.mobile })
            .sort({ _id: -1 })
            .lean()

        if (lastOTP && moment().isBefore(moment(lastOTP.createdAt).add(60, 'seconds'))) {
            throw "FREQUENT"
        }

        const otp = Helper.RandomNumber(4)
        await new OTPModel({ draft_id: payload.draft_id, mobile: payload.mobile, otp }).save()
        SMSService.SendOTP(payload.mobile, otp)

    } catch (error) {
        throw error
    }

}

service.validate = async (user, payload) => {

    try {

        const lastOTP = await OTPModel.findOne({ draft_id: payload.draft_id, mobile: payload.mobile })
            .sort({ _id: -1 })
            .lean()

        if (lastOTP && lastOTP.otp == payload.otp) {
            return true
        }

        throw "INVALID"

    } catch (error) {
        throw error
    }

}

service.isVerified = async (user) => {
    const userData = await UsersModel.findOne({ mobile: user.mobile, draft_id: user.draft_id })
    return userData.isVerified
}