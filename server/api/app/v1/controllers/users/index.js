const router = require('express').Router()
const controller = require('./users.controller')
const auth = require('../../../../../helpers/auth.helper')
/**
 * 
 * @register    Register
 * 
 */

router.post('/register', controller.register)
router.post('/login', controller.login)
router.get('/logout', auth.authenticate, controller.logout)
router.put('/', auth.authenticate, controller.update)
router.post('/verify', controller.verify)
router.post('/validate', controller.validate)

module.exports = router
