const PreferenceModel = require('../../../../models/preference')
const service = module.exports

service.save = async (user, type, payload) => {
    return await PreferenceModel.findOneAndUpdate({ user, type }, payload, { upsert: true })
}