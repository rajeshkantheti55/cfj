const router = require('express').Router()
const controller = require('./preferences.controller')

/**
 * 
 * @language    Save language preference
 * 
 */

router.post('/language/save', controller.language)

module.exports = router
