const service = require('./languages.service')
const LanguageDataService = require('../languages-data/languages-data.service')
const controller = module.exports

controller.language = async (req, res) => {
    try {

        const { user, body } = req
        const data = await service.save(body.draft_id, 'language', { languageId: body.languageId })
        const languageData = await LanguageDataService.data(body.languageId)
        const placeholders = {}

        for (const iterator of languageData) {
            if (iterator.isArray) {
                placeholders[iterator.key] = []
                for (const j of iterator.value) {
                    placeholders[iterator.key].push({ [j.key]: j.value })
                }
            } else {
                placeholders[iterator.key] = iterator.value
            }
        }

        return res.status(200).json({
            statusCode: 200,
            status: true,
            next_screen: "main",
            data: placeholders
        })

    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}