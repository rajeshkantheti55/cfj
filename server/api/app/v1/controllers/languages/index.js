const router = require('express').Router()
const controller = require('./languages.controller')

/**
 * 
 * @read    List languages
 * 
 */

router.get('/', controller.read)

module.exports = router
