const LanguageModel = require('../../../../models/languages')
const PreferenceModel = require('../../../../models/preference')
const service = module.exports

service.read = async (data) => {
    return await LanguageModel.find({}, {
        isDeleted: false,
        publish: false,
        createdAt: false,
        updatedAt: false,
        rank: 0,
        __v: 0
    }).sort({ id: 1 })
}

service.saveUserPreference = async (user, language) => {
    return await PreferenceModel.findOneAndUpdate({ user, type: 'language' }, { language })
}