const service = require('./languages.service')
const controller = module.exports

controller.read = async (req, res) => {
    try {
        const { user } = req
        const data = await service.read()
        return res.status(200).json({ statusCode: 200, status: true, data })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: 'Internal server error', error })
    }
}