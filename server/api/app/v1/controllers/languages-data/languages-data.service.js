const LanguageDataModel = require('../../../../models/languages-data')
const LanguageModel = require('../../../../models/languages')
const service = module.exports

service.data = async (language) => {
    const languageId = await LanguageModel.findOne({ id: language }).lean()
    return await LanguageDataModel.find({ language: languageId._id, isDeleted: false }).lean()
}