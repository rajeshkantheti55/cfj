const DeviceModel = require('../../../../models/devices')
const CounterHelper = require('../../../../../helpers/counters')
const service = module.exports

service.save = async (payload) => {

    let data = await DeviceModel.findOne({ device_id: payload.device_id })

    if (!data) {
        const draftId = await CounterHelper.get('draftId')
        data = await new DeviceModel({ ...payload, draft_id: draftId }).save()
    }

    return {
        draft_id: data.draft_id,
        font_size: 11,
        next_screen: "language",
        show_animation: 0,
        app_update: true,
        language_id: 0,
        static_data_api: 0,
        is_login: false,
        update_menu_status: 1,
        event_status: false
    }

}