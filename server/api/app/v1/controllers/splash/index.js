const router = require('express').Router()
const controller = require('./splash.controller')

/**
 * 
 * @save        Save
 * @animation   Animation
 * 
 */

router.post('/', controller.save)
router.post('/animation', controller.animation)

module.exports = router
