const joi = require('joi')
const SplashService = require('./splash.service')
const Schemas = require('../../schemas')
const controller = module.exports

controller.save = async (req, res) => {
    try {

        const { body } = req
        const { error, value } = joi.validate(body, Schemas.splash.save)

        if (error) {
            return res.status(400).json({ message: "Bad params", error })
        }

        const data = await SplashService.save(value)

        return res.status(200).json({ status: true, data })

    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })
    }
}

exports.animation = async (req, res) => {
    try {

        const { user } = req
        const { _id: userId } = user
        const userData = await SplashService.getByIdSync(userId)
        const { fullName } = userData
        return res.status(200).json({
            user: {
                ...user,
                fullName
            }
        })
    } catch (error) {
        console.error(error)
        return res.status(200).json({ status: false, message: "Internal server error", error })
    }
}