const joi = require('joi')
const validator = module.exports

validator.splash = {
    save: joi.object().unknown(true),
    update: joi.object().unknown(true)
}

validator.user = {
    save: joi.object({
        confirmPassword: joi.any()
            .valid(joi.ref('password'))
            .required()
            .options({ language: { any: { allowOnly: 'must match password' } } })
            .strip()
    }).unknown(true),
    update: joi.object().unknown(true),
    verify: joi.object().unknown(true),
    validate: joi.object({
        draft_id: joi.any(),
        mobile: joi.any(),
        otp: joi.number().required()
    }).unknown(true),
    login: joi.object({
        mobile: joi.any(),
        password: joi.any()
    }).unknown(true),
    logout: joi.object({
        draft_id: joi.any(),
        mobile: joi.any()
    }).unknown(true)
}