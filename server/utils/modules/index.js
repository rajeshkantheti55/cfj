const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');

module.exports = {
    express,
    mongoose,
    cors,
    bodyParser,
    path
}