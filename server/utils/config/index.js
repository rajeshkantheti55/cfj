const dotenv = require('dotenv')
const { parsed } = dotenv.config()

const config = {
    env: parsed.ENV,
    port: Number(parsed.PORT),
    database: {
        mongo: {
            uri: parsed.MONGO_URI,
            debug: true,
            options: {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useCreateIndex: true,
                useFindAndModify: false
            }
        }
    },
    keys: {
        secret: parsed.SECRET_KEY
    }
}

module.exports = config