const modules = require('./modules')
const config = require('./config')
const constants = require('./constants')

module.exports.initialize = () => {

    const utils = {
        modules,
        config,
        constants
    }

    return utils

}