const LIMITConstants = {
    DEFAULT_RECORDS_LIMIT: 20,
    MAX_RECORDS_LIMIT: 1000
}

const CRONConstants = {
    TASK_ASSIGN_INTERVAL: "*/10 * * * * *"
}

module.exports = {
    LIMITConstants,
    CRONConstants
}