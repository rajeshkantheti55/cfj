const CountersModel = require('../../api/models/counters')
const service = module.exports

service.get = async (type) => {
    const data = await CountersModel.findOneAndUpdate({ type }, { $inc: { count: 1 } }, { upsert: true, new: true })
    return data.count
}