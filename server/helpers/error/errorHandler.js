/* custom Error constructor which extends the JavaScript Error constructor. */
class ErrorHandler extends Error {
    constructor(statusCode, message) {
      super();
      this.statusCode = statusCode;
      this.message = message;
    }
  
  }
  
  const errorMap = {
    400: 'Bad Request',
    401: 'Unauthorized',
    404: 'Not Found',
    500: 'Internal Server Error'
  }
  
  const handleError = (err, res, custom = {}) => {
    const { statusCode, error, message } = err;
    const code = statusCode ? statusCode : 500
    res.status(code).json({
      statusCode: code,
      error: message ? message : errorMap[code] ? errorMap[code] : "Internal Server Error",
      message: custom.message ? custom.message : 'Internal Server Error'
    });
  };
    
  module.exports = {
    ErrorHandler,
    handleError,
    // generateBadRequestResponse
  }