const msg91 = require("msg91");
const axios = require("axios");
const config = require("../../../config/env");
const SMS_CLIENT = msg91(config.sms.authKey, config.sms.senderId, config.sms.route);

const sms = module.exports

sms.send = async (mobile, message) => {
    try {
        return SMS_CLIENT.send(mobile, message, (error) => {
        });
    } catch (error) {
        console.error("MSG91 error----->", error);
    }
};

sms.balance = async () => {
    try {
        let request_options = {
            url: '/balance.php',
            method: 'get',
            baseURL: config.msg91.host,
            params: {
                type: config.msg91.route_no,
                authkey: config.msg91.authkey
            }
        };
        let Response = await axios(request_options);
        if (Response.status == 200) {
            let Data = Response.data;
            resolve(Data);
        } else if (Response.status == 400) {
            console.error("MSG91_ERROR------->", Response);
            reject({ success: false, extras: { msg: "MSG91 ERROR" } });
        } else if (Response.status == 401) {
            console.error("MSG91_ERROR------->", Response);
            reject({ success: false, extras: { msg: "MSG91 ERROR" } });
        }
    } catch (error) {
        console.error("MSG91_ERROR Error-->", error);
        reject({ success: false, extras: { msg: "MSG91 ERROR" } });
    }
}