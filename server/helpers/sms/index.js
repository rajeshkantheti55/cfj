const sms = require("./msg91");

const messages = module.exports

messages.SendOTP = async (mobile, OTP, MessageID = '4er54r') => {
    try {
        let message = `Welcome to AnyNews, Your OTP for login is ${OTP}. Message-ID : ${MessageID}. Do not share this OTP with others for security reasons.`;
        return await sms.send(mobile, message);
    } catch (error) {
        reject(await CommonController.Common_Error_Handler(error));
    }
}

messages.provider = async (mobile, Message) => {
    try {
        let ProviderData = await CommonController.Common_Find_Default_SMS_Provider();
        if (ProviderData == null) {
            let Result = await MSG91Controller.Send_SMS(mobile, Message);
            resolve(Result);
        } else {
            if (ProviderData.Service_Type == 1) {
                //Msg91
                let Result = await MSG91Controller.Send_SMS(mobile, Message);
                resolve(Result);
            } else {
                // In future we have to implement other providers
                let Result = await MSG91Controller.Send_SMS(mobile, Message);
                resolve(Result);
            }
        }
    } catch (error) {
        reject(await CommonController.Common_Error_Handler(error));
    }
}
