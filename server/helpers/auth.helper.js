const jsonwebtoken = require('jsonwebtoken')
const helper = module.exports

helper.createToken = (data, options) => {
    const options2 = options || {}
    return jsonwebtoken.sign(data, 'c@llforj0b6', options2)
}

helper.verifyToken = (data) => {
    return jsonwebtoken.verify(data, 'c@llforj0b6')
}

helper.authenticate = (req, res, next) => {
    try {

        const { headers } = req
        const data = jsonwebtoken.verify(headers.authorization, 'c@llforj0b6')

        if (data) {
            req.user = data
            return next()
        }

        res.status(200).json({ status: false, message: 'Not authorized' })

    } catch (error) {
        res.status(200).json({ status: false, message: 'Not authorized' })
    }
}
