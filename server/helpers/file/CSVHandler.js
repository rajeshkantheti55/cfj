const FastCSV = require('fast-csv');
const fs = require('fs')

exports.read = function (path) {
    return fs.createReadStream(path).pipe(FastCSV.parse({ headers: true }))
}

exports.write = (writeData, outputPath) => new Promise(function (resolve, reject) {
    FastCSV.writeToPath(outputPath, writeData).on('error', (error) => {
        return reject(error)
    }).on('finish', () => {
        return resolve()
    })
})
