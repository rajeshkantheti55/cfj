const fs = require('fs')
const formidable = require('formidable')
const CSVHandler = require('./CSVHandler')
const helper = module.exports

helper.csv = function (req, res, next) {
    const form = new formidable.IncomingForm();
    form.parse(req, async (err, fields, files) => {
        if (err) {
            return res.status(200).json({ message: 'Internal server error' })
        }
        const csvData = []
        CSVHandler.read(files.file.path)
            .on('data', async (data) => {
                csvData.push(data)
            })
            .on('end', async () => {
                req.body.uploadData = { data: csvData, fields }
                fs.unlinkSync(files.file.path)
                return next()
            })
    })
}