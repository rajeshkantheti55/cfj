const crypto = require('crypto')
const helper = module.exports

helper.getIp = (req) => {
    try {
        let IPAddress = req.headers["x-forwarded-for"];
        if (IPAddress) {
            let list = IPAddress.split(",");
            IPAddress = list[list.length - 1];
        } else {
            IPAddress = req.connection.remoteAddress;
        }
        IPAddress = (IPAddress == '::1') ? '0:0:0:0' : IPAddress;
        return (IPAddress);
    } catch (error) {
        return { success: false, msg: "Internal error" }
    }
}

helper.RandomNumber = (length) => {
    return Math.floor(Math.pow(10, length - 1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1));
}

helper.getPasswordHash = (password) => {
    const salt = crypto.randomBytes(16).toString('base64')
    return `${salt}$f$${crypto
        .pbkdf2Sync(password, Buffer.from(salt, 'base64'), 10000, 64, 'sha512')
        .toString('base64')}`
}

helper.verifyPasswordHash = (password, passwordHash) => {
    if (!password || !passwordHash) {
        return false
    }
    const [salt, hash] = passwordHash.split('$f$')
    const cHash = crypto
        .pbkdf2Sync(password, Buffer.from(salt, 'base64'), 10000, 64, 'sha512')
        .toString('base64')
    return cHash === hash
}

helper.parsePageNoAndSize = (query) => {
    let pageNo = query.hasOwnProperty("pageNo") && Number(query.pageNo) > 0 ? Number(query.pageNo) - 1 : 0;
    let pageSize = query.hasOwnProperty("pageSize") ? Number(query.pageSize) : 10;
    return { pageNo, pageSize };
}