module.exports.initialize = async (input = {}) => new Promise((resolve, reject) => {

    try {

        const { modules, config } = input
        const { express, bodyParser, cors, path } = modules

        const app = express();

        app.listen(config.port, function (error) {

            if (error) {
                throw error
            }

            app.use(cors())
            app.set('view engine', 'html');
            app.use(bodyParser.json({ limit: '50mb' }));
            app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
            app.use(express.static(path.join(__dirname, '../dist')));
            app.set('client', path.join(__dirname, '../dist/index.html'));
            app.set('view engine', 'html');

            require('./api/routes.main')(app)

            return resolve()

        });

    } catch (error) {
        return reject(error)
    }

})